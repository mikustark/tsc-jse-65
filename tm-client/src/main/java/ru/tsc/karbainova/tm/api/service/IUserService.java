package ru.tsc.karbainova.tm.api.service;

import ru.tsc.karbainova.tm.endpoint.Role;
import ru.tsc.karbainova.tm.endpoint.UserDTO;

import java.util.List;

public interface IUserService {

    void clear();

    void addAll(List<UserDTO> users);

    boolean isLoginExists(String login);

    boolean isEmailExists(String email);

    List<UserDTO> findAll();

    UserDTO findById(String id);

    UserDTO findByLogin(String login);

    UserDTO removeUser(UserDTO user);

    UserDTO removeById(String id);

    UserDTO removeByLogin(String login);

    UserDTO create(String login, String password);

    UserDTO create(String login, String password, String email);

    UserDTO create(String login, String password, Role role);

    UserDTO setPassword(String userId, String password);

    UserDTO updateUser(
            String userId,
            String firstName,
            String lastName,
            String middleName);

    UserDTO lockUserByLogin(String login);

    UserDTO unlockUserByLogin(String login);
}
