package ru.tsc.karbainova.tm.listener.serv;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.karbainova.tm.event.ConsoleEvent;
import ru.tsc.karbainova.tm.listener.AbstractSystemListener;

@Component
public class VersionDisplayListener extends AbstractSystemListener {
    @Override
    public String name() {
        return "version";
    }

    @Override
    public String arg() {
        return "-v";
    }

    @Override
    public String description() {
        return "Version";
    }

    @Override
    @EventListener(condition = "@versionDisplayListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        System.out.println(propertyService.getApplicationVersion());
        System.out.println(Manifests.read("build"));
    }
}
