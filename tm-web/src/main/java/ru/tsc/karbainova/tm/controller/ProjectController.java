package ru.tsc.karbainova.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.tsc.karbainova.tm.enumerated.Status;
import ru.tsc.karbainova.tm.model.Project;
import ru.tsc.karbainova.tm.repository.ProjectRepository;

@Controller
public class ProjectController {

    @Autowired
    private ProjectRepository projectRepository;

    @GetMapping("/project/create")
    public String create() {
        projectRepository.save(new Project("new project"));
        return "redirect:/projects";
    }

    @GetMapping("/project/delete/{id}")
    public String delete(@PathVariable("id") String id) {
        projectRepository.removeById(id);
        return "redirect:/projects";
    }

    @GetMapping("/project/edit/{id}")
    public ModelAndView edit(@PathVariable("id") String id, Model model) {
        return new ModelAndView(
                "project-create",
                "project", projectRepository.findById(id)
        );
    }

    @PostMapping("/project/edit")
    public String edit(@ModelAttribute("project") Project project) {
        projectRepository.save(project);
        return "redirect:/projects";
    }

    @ModelAttribute("viewName")
    public String getViewName() {
        return "Project edit";
    }

    @ModelAttribute("statuses")
    public Status[] getStatuses() {
        return Status.values();
    }
}
