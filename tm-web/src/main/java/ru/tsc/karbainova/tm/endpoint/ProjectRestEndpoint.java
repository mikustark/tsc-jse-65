package ru.tsc.karbainova.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.tsc.karbainova.tm.api.IProjectRestEndpoint;
import ru.tsc.karbainova.tm.model.Project;
import ru.tsc.karbainova.tm.repository.ProjectRepository;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/projects")
public class ProjectRestEndpoint implements IProjectRestEndpoint {

    @Autowired
    private ProjectRepository repository;

    @Override
    @GetMapping("/findAll")
    public List<Project> findAll() {
        return new ArrayList<>(repository.findAll());
    }

    @Override
    @GetMapping("/find/{id}")
    public Project find(@PathVariable("id") String id) {
        return repository.findById(id);
    }

    @Override
    @PostMapping("/create")
    public Project create(@RequestBody Project project) {
        repository.save(project);
        return project;
    }

    @Override
    @PostMapping("/save")
    public Project save(@RequestBody Project project) {
        repository.save(project);
        return project;
    }

    @Override
    @PostMapping("/delete/{id}")
    public void delete(@PathVariable("id") String id) {
        repository.removeById(id);
    }

}
