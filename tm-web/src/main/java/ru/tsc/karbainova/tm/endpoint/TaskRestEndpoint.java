package ru.tsc.karbainova.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.tsc.karbainova.tm.api.ITaskRestEndpoint;
import ru.tsc.karbainova.tm.model.Task;
import ru.tsc.karbainova.tm.repository.TaskRepository;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/tasks")
public class TaskRestEndpoint implements ITaskRestEndpoint {

    @Autowired
    private TaskRepository repository;

    @Override
    @GetMapping("/findAll")
    public List<Task> findAll() {
        return new ArrayList<>(repository.findAll());
    }

    @Override
    @GetMapping("/find/{id}")
    public Task find(@PathVariable("id") String id) {
        return repository.findById(id);
    }

    @Override
    @PostMapping("/create")
    public Task create(@RequestBody Task task) {
        repository.save(task);
        return task;
    }

    @Override
    @PostMapping("/save")
    public Task save(@RequestBody Task task) {
        repository.save(task);
        return task;
    }

    @Override
    @PostMapping("/delete/{id}")
    public void delete(@PathVariable("id") String id) {
        repository.removeById(id);
    }
}
